genshin_zip="genshin"
genshin_character_zip="genshin_character.zip"
list_character="list_character.csv"

unzip "$genshin_zip"

  if [ $? -eq 0 ]; then
    	echo "Unzip '$genshin_zip' berhasil."
  else
    	echo "Error: Gagal unzip '$genshin_zip'."
       	exit 1
  fi

  if [ ! -f "$genshin_character_zip" ]; then
   	 echo "Error: File '$genshin_character_zip' tidak ada."
    	exit 1
  fi

unzip "$genshin_character_zip"

  if [ $? -eq 0 ]; then
   	 echo "Unzip '$genshin_character_zip' berhasil."
  else
   	 echo "Error: Gagal unzip '$genshin_character_zip'."
    	exit 1
  fi

mkdir -p genshin_character

for encoded_filename in *.jpg; do
    decoded_filename=$(echo "$encoded_filename" | base64 -d 2>/dev/null)
    cocoklogi=$(grep -F "$decoded_filename" "$list_character")

    if [ -z "$cocoklogi" ]; then
        echo "Error: Tidak ada baris yang cocok untuk '$decoded_filename' dalam '$list_character'."
        exit 1
    fi

    nama=$(echo "$cocoklogi" | awk -F',' '{gsub("\r",""); print $1}')
    region=$(echo "$cocoklogi" | awk -F',' '{gsub("\r",""); print $2}')
    elemen=$(echo "$cocoklogi" | awk -F',' '{gsub("\r",""); print $3}')
    senjata=$(echo "$cocoklogi" | awk -F',' '{gsub("\r",""); print $4}')

    mkdir -p "genshin_character/$region"

    namacharacter="${nama}-${region}-${elemen}-${senjata}"
    mv "$encoded_filename" "genshin_character/$region/${namacharacter}.jpg"
    echo "Merename: $encoded_filename -> genshin_character/$region/${namacharacter}.jpg"

done

for weapon in Catalyst Sword Claymore Bow Polearm; do
        count=$(find genshin_character/ -name "*$weapon.jpg" | wc -l)
        echo "nama senjata $weapon : $count"
done

rm genshin_character.zip
rm genshin.zip
rm list_character.csv
