#!/bin/bash

for region in genshin_character/*; do
 if [ -d "$region" ]; then
    for image in "$region"/*; do
        image_filename=$(echo "$image" | awk -F ' -' '{print $1}')
        image_basename=$(echo "$image_filename" | awk -F '/' '{print $3}')
        steghide extract -sf "$image" -p "" -xf "${image_basename}.txt"
                decoded_message=$(cat "${image_basename}.txt" | base64 --decoded)
                timestamp=$(date '+%d/%m/%y %H:%M:%S')

if [[ "$decoded_message" == http ]]; then
        wget "$decoded_message"
        echo "[$timestamp] [FOUND] [$image]" >> image.log
        pkill -f "find_me.sh"
else
        echo "[$timestamp] [NOT FOUND] [$image]" >> image.log
        rm "${image_basename}.txt"
fi
  sleep 1
done

fi

done

