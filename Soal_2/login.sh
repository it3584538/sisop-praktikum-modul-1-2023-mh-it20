#!/bin/bash

# Meminta input dari pengguna
read -p "Masukkan email: " email
read -s -p "Masukkan password: " password
echo

# Cek apakah email sudah terdaftar
if grep -q "$email" users.txt; then
  # Ambil username dari email
  username=$(grep "$email" users.txt | awk '{print $2}')
  # Ambil password terenkripsi dari file
  encrypted_password=$(grep "$email" users.txt | awk '{print $3}')
  
  # Dekripsi password dan cocokkan
  decrypted_password=$(echo -n "$encrypted_password" | base64 -d)
  if [ "$decrypted_password" = "$password" ]; then
    echo "LOGIN SUCCESS - Welcome, $username"
    log="[LOGIN] [$(date +'%d/%m/%y %H:%M:%S')] [LOGIN SUCCESS] user $username logged in successfully"
  else
    echo "LOGIN FAILED - email $email not registered, please register first"
    log="[LOGIN] [$(date +'%d/%m/%y %H:%M:%S')] [LOGIN FAILED] ERROR Failed login attempt on user with email $email"
  fi
else
  echo "LOGIN FAILED - email $email not registered, please register first"
  log="[LOGIN] [$(date +'%d/%m/%y %H:%M:%S')] [LOGIN FAILED] ERROR Failed login attempt on user with email $email"
fi

# Catat log
echo "$log" >> auth.log
