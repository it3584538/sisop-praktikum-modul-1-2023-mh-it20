#!/bin/bash

# Meminta input dari pengguna
read -p "Masukkan email: " email
read -p "Masukkan username: " username
read -s -p "Masukkan password: " password
echo

# Memeriksa apakah email sudah terdaftar
if grep -q "$email" users.txt; then
  echo "Email sudah terdaftar. Silakan gunakan email lain."
else
  # Menyimpan informasi ke dalam file users.txt
  echo "$email:$username:$password" >> users.txt
  echo "Registrasi berhasil!"
fi
