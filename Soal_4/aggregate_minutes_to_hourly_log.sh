#!/bin/bash

# Direktori log
log_dir="/home/yanuar/nomor4"

# Waktu saat ini untuk agregasi per jam
hourly_timestamp=$(date +'%Y%m%d%H')

# Menggabungkan semua file log per menit dalam satu jam
cat "$log_dir/metrics_$hourly_timestamp"* > "$log_dir/metrics_agg_$hourly_timestamp.log"

# Menghitung nilai minimum, maksimum, dan rata-rata dari metrik RAM dan Disk
min_ram=$(awk -F',' 'NR>1{print $2}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -n | head -n 1)
max_ram=$(awk -F',' 'NR>1{print $2}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -n | tail -n 1)
avg_ram=$(awk -F',' 'NR>1{sum+=$2}END{print sum/(NR-1)}' "$log_dir/metrics_agg_$hourly_timestamp.log")

min_disk=$(awk -F',' 'NR>1{print $12}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -h | head -n 1)
max_disk=$(awk -F',' 'NR>1{print $12}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -h | tail -n 1)
avg_disk=$(awk -F',' 'NR>1{sum+=$12}END{print sum/(NR-1)}' "$log_dir/metrics_agg_$hourly_timestamp.log")

# Simpan hasil agregasi dalam satu file
echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_cache,mem_available,swap_total,swap_used,swap_free,swap_cache,path,path_size" > "$log_dir/metrics_agg_$hourly_timestamp.log"
echo "minimum,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_disk" >> "$log_dir/metrics_agg_$hourly_timestamp.log"
echo "maximum,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_disk" >> "$log_dir/metrics_agg_$hourly_timestamp.log"
echo "average,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_disk" >> "$log_dir/metrics_agg_$hourly_timestamp.log"
