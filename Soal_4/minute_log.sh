#!/bin/bash

# Log directori
log_dir="/home/yanuar/nomor4"

# Pengaturan waktu yang akan digunakan
timestamp=$(date +'%Y%m%d%H%M%S')

# Deklarasi Ram dan swap dengan 'free -m'
ram_info=$(free -m | awk 'NR==2{print $2","$3","$4","$5","$6","$7}')
swap_info=$(free -m | awk 'NR==3{print $2","$3","$4}')

# Target path 'du -sh'
target_path="/home/yanuar/"
dir_size_info=$(du -sh "$target_path" | awk '{print $1}')

# Menyimpan Source monitoring
echo "$ram_info,$swap_info,$target_path,$dir_size_info" >> "$log_dir/metrics_$timestamp.log"
